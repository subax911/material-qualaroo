import { Component, TemplateRef } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';

@Component({
  selector: 'app-dialog-button',
  templateUrl: './dialog-button.component.html',
  styleUrls: ['./dialog-button.component.scss']
})
export class DialogButtonComponent {

  constructor(
    private readonly dialog: MatDialog,
  ) { }

  openDialog(template: TemplateRef<HTMLElement>): void {
    this.dialog.open(template);
  }

}
